import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {Actions,}from 'react-native-router-flux';
class Home extends React.Component {
    state = {name: '',};

    render() {
        return (
            <View>
                <Text style={styles.title}>
                    Enter Your Name:
                </Text>
                <TextInput
                    style={styles.nameInput}
                    placeholder='Nour name'
                    onChangeText={(text) => {
                        this.setState({
                            name: text,
                        })
                    }}
                />
                <TouchableOpacity
                    onPress={() => {
                        Actions.chat({
                            name: this.state.name,
                        })
                    }}
                >
                    <Text style={styles.buttonText}>
                        Next
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    title: {
        marginTop: 20,
        marginLeft: 20,
        fontSize: 20,
        color: 'green'
    },
    nameInput: {
        padding: 10,
        height: 40,
        borderWidth: 1,
        borderColor: 'black',
        margin: 20,
    },
    buttonText: {
        margin: 20,
        fontSize: 16,
        backgroundColor: 'gray',
        padding: 10,
        width: 60,
    }

});
export  default Home;